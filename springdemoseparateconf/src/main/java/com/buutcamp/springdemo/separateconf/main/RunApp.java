package com.buutcamp.springdemo.separateconf.main;


import com.buutcamp.springdemo.separateconf.Objects.Person;
import com.buutcamp.springdemo.separateconf.config.AppConfigMain;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class RunApp {

    public RunApp() {
        //do something
        ApplicationContext appCtx = new AnnotationConfigApplicationContext(AppConfigMain.class);

        //Beans can also be called using the class type, e.g., Person.class as argument
        //However, using IDs is best practice
        Person person = appCtx.getBean("personBeanId_1",Person.class);

        //Properties for this bean has been set in AppConfig1
        System.out.println("Person 1 bean name = " + person.getName());
        System.out.println("Person 1 bean age = " + person.getAge());

        person = appCtx.getBean("personBeanId_2",Person.class);

        //Properties for this bean has been set in AppConfig2
        System.out.println("Person 2 bean name = " + person.getName());
        System.out.println("Person 2 bean age = " + person.getAge());

        ((AnnotationConfigApplicationContext) appCtx).close();

    }
}
