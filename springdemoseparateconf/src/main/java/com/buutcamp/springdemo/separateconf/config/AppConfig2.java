package com.buutcamp.springdemo.separateconf.config;

import com.buutcamp.springdemo.separateconf.Objects.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class AppConfig2 {

    @Bean(name="personBeanId_2",initMethod = "init",destroyMethod = "destroy")
    @Scope("singleton")
    public Person person2() {
        Person person = new Person();
        person.setAge(16);
        person.setName("My name is Bean_2!");
        return person;
    }
}
