package com.buutcamp.springdemo.separateconf.config;

import com.buutcamp.springdemo.separateconf.Objects.Person;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class AppConfig1 {

    @Bean(name="personBeanId_1",initMethod = "init",destroyMethod = "destroy")
    @Scope("singleton")
    public Person person1() {
        Person person = new Person();
        person.setAge(24);
        person.setName("My name is Bean_1!");
        return person;
    }

}
